function redirectMobileHandler() {
    const width = Math.max(document.clientWidth || 0, window.innerWidth || 0);
    if(width > 400) {
      window.location = '../../';
    }
  }
  
  window.onload = redirectMobileHandler();
  window.onresize = () => redirectMobileHandler();