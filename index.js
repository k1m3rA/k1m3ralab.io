const letters = "ABCDEFGHIJKLMNÑOPQRSTUVXYZÇ";

document.querySelectorAll(".sub").forEach(
  (link) =>
    (link.onmouseover = (event) => {
      let iterations = 0;
      let sleep = 4;
      if (event.target.dataset.value.length > 12) {
        sleep = 2;
      }
      if (event.target.dataset.value.length < 5) {
        sleep = 20;
      }
      const interval = setInterval(() => {
        event.target.innerText = event.target.innerText
          .split("")
          .map((letter, index) => {
            if (index < iterations) {
              return event.target.dataset.value[index];
            }
            return letters[
              Math.floor(Math.random() * parseInt(letters.length))
            ];
          })
          .join("");
        if (iterations >= event.target.dataset.value.length)
          clearInterval(interval);
        iterations += 1 / sleep;
      }, 30);
    })
);

function redirectMobileHandler() {
  const width = innerWidth;
  if(width <= 978) {
    window.location = 'mobile/';
  }
}

window.onload = redirectMobileHandler();
window.onresize = () => redirectMobileHandler();

if(document.querySelector(".sub-button")){
  document.querySelector(".sub-button").addEventListener("click", (event) => {
    navigator.clipboard.writeText(event.target.dataset.value);
  });
}

